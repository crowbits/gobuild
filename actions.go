package main

import (
	actInit "bitbucket.org/crowbits/gobuild/packages/action_init"
	actPub "bitbucket.org/crowbits/gobuild/packages/action_publish"
	ver "bitbucket.org/crowbits/gobuild/packages/versions"
)

func runInit() error {
	// Create New Init Action
	myInit, err := actInit.NewActInit()
	if err != nil {
		return err
	}

	// Set Geneic App Info
	myInit.GetAppInfo()

	// Set Go Version
	myInit.NewConfig.Tools.Go.Version = ver.GetCurrentGoVersion()

	// Save Conf
	return myInit.SaveConf()
}

func funPublish() error {
	myPub, err := actPub.NewPublisher()
	if err != nil {
		return err
	}

	myPub.Publish()

	return nil
}
