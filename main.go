package main

import (
	"fmt"
	"os"

	actionBuild "bitbucket.org/crowbits/gobuild/packages/action_build"
	msg "bitbucket.org/crowbits/gobuild/packages/msging"
)

func init() {
}

func main() {
	if len(os.Args) <= 1 {
		os.Args = append(os.Args, "build")
	}

	switch os.Args[1:2][0] {
	default:

		actBuild, err := actionBuild.NewActBuild(os.Args[1:])
		if err != nil {
			msg.Error(err)
			os.Exit(2)
		}

		displayAppInfo(actBuild.Config.Application.Name, actBuild.Config.Application.Version)
		actBuild.BuildProject()

	case "init":
		err := runInit()
		if err != nil {
			msg.Error(err)
			os.Exit(10)
		}
	case "publish":
		err := funPublish()
		if err != nil {
			msg.Error(err)
			os.Exit(10)
		}
	case "help", "-h":
		if len(os.Args) <= 2 {
			displayHelpCommands()
			os.Exit(2)
		}
		switch os.Args[2:3][0] {
		default:
			fmt.Println(msg.ColRed("Unknown Command passed"))
			fmt.Println("")
			displayHelpCommands()
			os.Exit(2)
		case "init":
			helpDisplayInit()
		case "build":
			helpDisplayBuild()
		case "publish":
			helpDisplayPublish()
		}
	}
}
