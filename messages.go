package main

import (
	"fmt"

	msg "bitbucket.org/crowbits/gobuild/packages/msging"
)

/******************************************************************************
 * App Info
 ******************************************************************************/
func displayAppInfo(appName, appVersion string) {

	fmt.Println("")
	fmt.Println(
		msg.ColMagenta(appName),
		msg.GetPadding(70, fmt.Sprint(appName, appName, "[  ]"), " "),
		"[", msg.ColMagenta(appVersion), "]")
	fmt.Println(msg.ColMagentaBold(msg.GetPadding(70, "", "=")))
	fmt.Println("")

}
