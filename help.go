package main

import (
	"fmt"

	"github.com/fatih/color"
)

var colTitle = color.New(color.FgHiMagenta).Add(color.Bold).SprintFunc()
var colSection = color.New(color.FgWhite).Add(color.Bold).SprintFunc()
var colArgument = color.New(color.FgHiCyan).SprintFunc()
var colCommand = color.New(color.FgHiGreen).SprintFunc()

func displayHelpCommands() {
	fmt.Println("")
	fmt.Println(colTitle("GoBuild"))
	fmt.Println("")
	fmt.Println("GoBuild is an application written in go to help with the building of go applications.")
	fmt.Println("")
	fmt.Println(colSection("Usage:"))
	fmt.Println("")
	fmt.Println("    gobuild COMMAND [arguments]")
	fmt.Println("")
	fmt.Println(colSection("Commands:"))
	fmt.Println("")
	fmt.Printf("    %-20s  %s\n", colArgument("init"), "Initialize the current directory and a base for the project")
	fmt.Printf("    %-20s  %s\n", colArgument("build"), "Build(install) the current application")
	fmt.Printf("    %-20s  %s\n", colArgument("publish"), "Build the current application acording to specs in config file")
	//fmt.Printf("    %-20s  %s\n", colArgument("package"), "Take an action on a package")
	//fmt.Printf("    %-20s  %s\n", colArgument("tools"), "Take an action with a tool")
	fmt.Println("")
	fmt.Printf("Use %s for more information about a command.\n", colCommand("go help [command]"))
	fmt.Println("")
}

func helpDisplayInit() {
	fmt.Println("")
	fmt.Println(colTitle("Init Command"))
	fmt.Println("")
	fmt.Println("Init is used to generate the basic files needed for runing gobuild on your application. Also")
	fmt.Println("to start a bank folder as a gobuild appliction.")
	fmt.Println("")
	fmt.Println(colSection("Usage:"))
	fmt.Println("")
	fmt.Println("    gobuild init [flags]")
	fmt.Println("")
	fmt.Println(colSection("Flags:"))
	fmt.Println("")
	fmt.Println("    None at this time.")
	fmt.Println("")
}

func helpDisplayBuild() {
	fmt.Println("")
	fmt.Println(colTitle("Build Command"))
	fmt.Println("")
	fmt.Println("Build is used to compile/test/build the code base.")
	fmt.Println("")
	fmt.Println("Different paths will happen based on the flags below but by default each package ( located ./packages )")
	fmt.Println("will be ran with the 'install' command and then the base ( ./ ) will also be ran with install.")
	fmt.Println("after each build you will be notified of what ones if any have failed.")
	fmt.Println("")
	fmt.Println(colSection("Usage:"))
	fmt.Println("")
	fmt.Println("    gobuild build [flags]")
	fmt.Println("")
	fmt.Println(colSection("Flags:"))
	fmt.Println("")
	fmt.Printf("    %-25s  %s\n", colArgument("-a, --assets"), "If Assets are set force rebuild ( even if there is no difference )")
	fmt.Printf("    %-25s  %s\n", colArgument("-c, --coverage"), "Run coverage over each package and main application")
	fmt.Printf("    %-25s  %s\n", colArgument("-s, --skip"), "If Coverage or Test failes check dont error out")
	fmt.Printf("    %-25s  %s\n", colArgument("-t, --test"), "Run Tests over each package and main application")
	fmt.Println("")
}

func helpDisplayPublish() {
	fmt.Println("")
	fmt.Println(colTitle("Publish Command"))
	fmt.Println("")
	fmt.Println("Publish is used to generate a compiled version ( or versions ) of the code for deployment")
	fmt.Println("")
	//TODO: Fill this part in
	fmt.Println("Describe how to setup the the config file here")
	fmt.Println("")
	fmt.Println(colSection("Usage:"))
	fmt.Println("")
	fmt.Println("    gobuild publish [flags]")
	fmt.Println("")
	fmt.Println(colSection("Flags:"))
	fmt.Println("")
	fmt.Printf("    %-25s  %s\n", colArgument("-f, --force"), "If there is a postcmd in the config this will suppress the warning before running")
	fmt.Println("")
}
