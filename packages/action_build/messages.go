package actionBuild

import (
	"fmt"

	msg "bitbucket.org/crowbits/gobuild/packages/msging"
)

type message struct {
	cols map[string]column
}

type column struct {
	title   string
	width   int
	visable bool
}

var colOrder = []string{"package", "test", "build"}

func buildMessager(proc buildProcs) (newMsg message) {
	newMsg.cols = make(map[string]column)

	// Add Extra Columns
	newMsg.cols["test"] = column{title: "Test", width: 10, visable: proc.Test}
	newMsg.cols["build"] = column{title: "Build", width: 10, visable: true}

	// Get Total Col Widths
	additionalColWidthTot := 0
	for _, col := range newMsg.cols {
		if col.visable {
			additionalColWidthTot += col.width
		}
	}

	newMsg.cols["package"] = column{title: "Package", width: (fullRowWidth - additionalColWidthTot), visable: true}
	return
}

func (m *message) printBuildHeader() {
	for _, key := range colOrder {
		colItm := m.cols[key]
		if colItm.visable {
			fmt.Print(colBlue(colItm.getTitle()))
		}
	}
	fmt.Println("")

	// Under line it
	fmt.Println(colBlue(msg.GetPadding(fullRowWidth, "", "-")))
}

func (m *message) printPackageName(pkgName string) {
	if pkgName == "" {
		pkgName = "Main application"
	}
	pkgCol := m.cols[colOrder[0]]
	fmt.Print(colCyan(pkgName), msg.GetPadding(pkgCol.width, pkgName, " "))
}

func (m *message) printStatus(err error) {
	if err != nil {
		fmt.Print(colRed(statFail))
	} else {
		fmt.Print(colGreen(statSuccess))
	}
}

func (c *column) getTitle() string {
	return fmt.Sprint(c.title, msg.GetPadding(c.width, c.title, " "))
}
