package actionBuild

import (
	"fmt"
	"strings"
)

// BuildProject will run the build process
func (ab *ActBuild) BuildProject() (err error) {
	// Assests if Needed
	if ab.Process.Assets {
		fmt.Println("Process Assets")
	}

	// Get List of Packages
	ab.BuildList, err = ab.getPackages()
	if err != nil {
		return err
	}

	ab.messaging.printBuildHeader()

	for _, pack := range ab.BuildList {
		ab.messaging.printPackageName(strings.TrimPrefix(pack, "./"))

		ab.messaging.printStatus(ab.buildPackage(pack))

		fmt.Println("")
	}
	return err
}
