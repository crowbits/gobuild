package actionBuild

import "github.com/fatih/color"

const (
	fullRowWidth = 70
	statSuccess  = "[Success]"
	statFail     = "[Fail]"
)

var colRed = color.New(color.FgHiRed).SprintFunc()
var colGreen = color.New(color.FgHiGreen).SprintFunc()
var colCyan = color.New(color.FgHiCyan).SprintFunc()
var colBlue = color.New(color.FgHiBlue).SprintFunc()
