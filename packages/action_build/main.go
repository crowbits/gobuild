package actionBuild

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"strings"

	"bitbucket.org/crowbits/gobuild/packages/gobuild_config"
)

// ActBuild Holds all the info needed to run a build
type ActBuild struct {
	Args      []string
	Config    goBuildConfig.GoBuildConfig
	Process   buildProcs
	BuildList []string
	messaging message
}

type buildProcs struct {
	Assets   bool
	Coverage bool
	Skip     bool
	Test     bool
}

// NewActBuild will read in the settings for the new Action Build
func NewActBuild(args []string) (ActBuild, error) {
	// Create New Build struct
	act := ActBuild{}

	// Add Args
	act.Args = args

	// Load Config
	act.Config = goBuildConfig.GoBuildConfig{}
	if act.Config.Load() != goBuildConfig.ErrorNil {
		return act, fmt.Errorf("Unable to locate a config file for gobuild")
	}

	// Pars Flags
	err := act.parseFlags()

	// Setup Messages
	act.messaging = buildMessager(act.Process)

	// Return
	return act, err
}

func (ab *ActBuild) buildPackage(path string) (err error) {
	_, err = exec.Command("go", "install", path).Output()
	return err
}

func (ab *ActBuild) getPackages() (pList []string, err error) {
	havePackages, err := dirExists("./packages")
	if err != nil {
		return pList, err
	}

	if havePackages {
		files, err := ioutil.ReadDir("./packages")
		if err != nil {
			return pList, err
		}

		for _, file := range files {
			pList = append(pList, "./packages/"+strings.TrimSpace(file.Name()))
		}
	}

	pList = append(pList, "./")

	return pList, nil
}

func dirExists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return true, err
}
