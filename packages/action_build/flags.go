package actionBuild

import (
	"bytes"
	"flag"
)

func (ab *ActBuild) parseFlags() error {
	// Remove Build Flag if present
	if len(ab.Args) >= 1 && ab.Args[0] == "build" {
		ab.Args = ab.Args[1:]
	}

	// Setup Flag Set
	buildFlag := flag.NewFlagSet("build", flag.ContinueOnError)
	buf := bytes.NewBuffer([]byte{})
	buildFlag.SetOutput(buf)

	// Flag: Build Assets
	buildFlag.BoolVar(&ab.Process.Assets, "a", ab.Process.Assets, "")
	buildFlag.BoolVar(&ab.Process.Assets, "assets", ab.Process.Assets, "")

	// Flag: Test Coverage
	buildFlag.BoolVar(&ab.Process.Coverage, "c", ab.Process.Coverage, "")
	buildFlag.BoolVar(&ab.Process.Coverage, "coverage", ab.Process.Coverage, "")

	// Flag: Skip Errors when testing and or coverage
	buildFlag.BoolVar(&ab.Process.Skip, "s", ab.Process.Skip, "")
	buildFlag.BoolVar(&ab.Process.Skip, "skip", ab.Process.Skip, "")

	// Flag: Skip Errors when testing and or coverage
	buildFlag.BoolVar(&ab.Process.Test, "t", ab.Process.Test, "")
	buildFlag.BoolVar(&ab.Process.Test, "test", ab.Process.Test, "")

	// Pars Arguments
	buildFlag.Parse(ab.Args)

	// Validate
	//TODO: Add some validation on the params passed

	return nil
}
