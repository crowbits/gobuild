package msging

import (
	"fmt"

	"github.com/fatih/color"
)

// Available Colors
var (
	ColGreen       = color.New(color.FgGreen).SprintFunc()
	ColMagenta     = color.New(color.FgHiMagenta).SprintFunc()
	ColMagentaBold = color.New(color.FgHiMagenta).Add(color.Bold).SprintFunc()
	ColRed         = color.New(color.FgHiRed).SprintFunc()
	ColBlue        = color.New(color.FgBlue).SprintFunc()
	ColBlueHi      = color.New(color.FgHiBlue).SprintFunc()
)

// GetPadding will return the padding to make the passed text the size you want
func GetPadding(max int, txt, chr string) (rtrn string) {
	for i := 0; i < (max - len(txt)); i++ {
		rtrn = fmt.Sprint(rtrn, chr)
	}
	return
}

// Error will print an error msg to the terminal
func Error(msgInput error) {
	fmt.Printf("%s: %s\n", ColRed("[ERROR]"), msgInput)
}
