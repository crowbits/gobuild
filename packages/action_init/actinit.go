package actionInit

import (
	"fmt"
	"os"
	"path"

	gbconf "bitbucket.org/crowbits/gobuild/packages/gobuild_config"
)

// ActInit Base Struct
type ActInit struct {
	NewConfig gbconf.GoBuildConfig
	Error     error
}

// NewActInit will return a new ActInit
func NewActInit() (newInit ActInit, err error) {
	if newInit.NewConfig.Load() != gbconf.ErrorNoConfig {
		return newInit, fmt.Errorf("This application is already setup")
	}
	return
}

// GetAppInfo Will Pull genic info about the app
func (a *ActInit) GetAppInfo() {
	// Get Base Path
	curPath, err := os.Getwd()
	if err != nil {
		a.Error = err
		return
	}

	// Set Base App Info
	a.NewConfig.Application.Name = path.Base(curPath)
	a.NewConfig.Application.Version = "0.0.0"
}

// SaveConf will Check'n'Save
func (a *ActInit) SaveConf() error {
	if a.Error != nil {
		return a.Error
	}
	return a.NewConfig.Save()
}
