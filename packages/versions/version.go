package versions

import (
	"fmt"
	"os/exec"
	"strings"
)

// GetCurrentGoVersion will return the current version of go installed on machine
func GetCurrentGoVersion() string {
	ver, _ := pullVersion([]string{"go", "version"}, "version go")
	return ver
}

// VersionCheck will test if the base version is older or same as test version
func VersionCheck(baseVer, testVer string) bool {

	// Split version strins
	baseDat := strings.Split(baseVer, ".")
	testDat := strings.Split(testVer, ".")

	// Length must match
	if len(baseDat) != len(testDat) {
		return false
	}

	// Check if newer
	for idx, val := range baseDat {
		if val > testDat[idx] {
			return false
		}
	}

	return true
}

func pullVersion(cmd []string, parsStr string) (string, error) {
	out, err := exec.Command(cmd[0], cmd[1:]...).Output()
	if err != nil {
		return "", err
	}

	tmp := strings.Split(string(out), parsStr)
	if len(tmp) > 1 {
		tmp = strings.Split(tmp[1], " ")
		return tmp[0], nil
	}
	return "", fmt.Errorf("The Output was not what was expected")
}
