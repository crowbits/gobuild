package actpub

import (
	"fmt"
	"os"
	"os/exec"
	"syscall"
)

func (a *ActPublish) runCommand(cmdLine []string, env map[string]string, show bool) int {

	// Setup Env
	newEnv := os.Environ()
	for key, val := range env {
		newEnv = append(newEnv, fmt.Sprintf("%s=%s", key, val))
	}

	// Setup Command
	cmd := exec.Command(cmdLine[0], cmdLine[1:]...)
	cmd.Env = newEnv

	if show {
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
	}

	// Run Command and capture status
	var waitStatus syscall.WaitStatus
	if err := cmd.Run(); err != nil {
		if exitError, ok := err.(*exec.ExitError); ok {
			waitStatus = exitError.Sys().(syscall.WaitStatus)
		}
	} else {
		waitStatus = cmd.ProcessState.Sys().(syscall.WaitStatus)
	}

	return waitStatus.ExitStatus()
}
