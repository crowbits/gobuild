package actpub

import (
	"fmt"

	msg "bitbucket.org/crowbits/gobuild/packages/msging"
)

const (
	fullWidth       = 70
	statWidth       = 10
	hdrTitleColTxt  = "Application Name"
	hdrStatuscolTxt = "Published"
	statFail        = "[Failed]"
	statSucc        = "[Success]"
)

func (a *ActPublish) printHeader() {
	fmt.Print(msg.ColBlueHi(hdrTitleColTxt), msg.GetPadding(fullWidth-statWidth, hdrTitleColTxt, " "))
	fmt.Print(msg.ColBlueHi(hdrStatuscolTxt), msg.GetPadding(statWidth, hdrStatuscolTxt, " "))
	fmt.Println("")
	fmt.Println(msg.ColBlueHi(msg.GetPadding(fullWidth, "", "=")))
}

func (a *ActPublish) printPubTitle(title string) {
	dsplyTitle := fmt.Sprint(title, " ")
	fmt.Print(msg.ColMagenta(dsplyTitle), msg.GetPadding(fullWidth-statWidth, dsplyTitle, "."))
}

func (a *ActPublish) printStatus(stat int) int {
	switch stat {
	default:
		fmt.Println(msg.ColRed(statFail), msg.GetPadding(statWidth, statFail, " "))
	case 0:
		fmt.Println(msg.ColGreen(statSucc), msg.GetPadding(statWidth, statSucc, " "))
	}
	return stat
}

func (a *ActPublish) printErrorLine(errMsg string) {
	fmt.Println(msg.ColRed("[Error]: "), errMsg)
}
