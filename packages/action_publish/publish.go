package actpub

import (
	"fmt"
	"os"
	"strings"
)

func (a *ActPublish) Publish() {

	// Setup some base stuff
	var emptyMap map[string]string
	var hadError error

	a.printHeader()

	for _, pubData := range a.Config.Publish {

		// Display Title
		a.printPubTitle(pubData.Title)

		// Setup Arguments
		cmdArgs := []string{"go", "build", "-a", "-o", pubData.OutFile}
		// Build Install Suffix
		if pubData.InstallSufix != "" {
			cmdArgs = append(cmdArgs, "-installsuffix")
			cmdArgs = append(cmdArgs, pubData.InstallSufix)
		}

		// Run Build Command
		errBuild := a.printStatus(a.runCommand(cmdArgs, pubData.SysVars, false))
		if errBuild != 0 && hadError == nil {
			hadError = fmt.Errorf("Unable to build '%s' please fix and try again", pubData.Title)
		}

		// Run Post Command if Exists
		if pubData.PostCmd != "" {
			errCmd := a.runCommand(strings.Split(pubData.PostCmd, " "), emptyMap, true)
			if errCmd != 0 && hadError == nil {
				hadError = fmt.Errorf("Failed to run '%s' for %s", pubData.PostCmd, pubData.Title)
			}
		}

		if hadError != nil {
			a.printErrorLine(hadError.Error())
			os.Exit(1)
		}
	}
}
