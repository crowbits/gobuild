package actpub

import (
	"fmt"

	"bitbucket.org/crowbits/gobuild/packages/gobuild_config"
)

// ActPublish is the base struct for Publishing
type ActPublish struct {
	Args   []string
	Config goBuildConfig.GoBuildConfig
}

// NewPublisher will build a new publisher obj
func NewPublisher() (ActPublish, error) {
	pub := ActPublish{}

	// Load Config
	if pub.Config.Load() != goBuildConfig.ErrorNil {
		return pub, fmt.Errorf("Unable to locate a config file for gobuild")
	}

	return pub, nil
}
