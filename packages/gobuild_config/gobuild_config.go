package goBuildConfig

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

const (
	configPath = "./gobuild.json"
)

// ConfError is the standard for Config errors
type ConfError struct {
	error
	ExitCode int
}

// Possable errors from config
var (
	ErrorNil           = ConfError{fmt.Errorf(""), 0}
	ErrorNoConfig      = ConfError{fmt.Errorf("Unable to locate gobuild.json file in current directory"), 10}
	ErrorUnableToRead  = ConfError{fmt.Errorf("Unable to read the config file"), 11}
	ErrorUnableToParse = ConfError{fmt.Errorf("Unable to parse config file"), 12}
)

// Load will open the current config file for use
func (conf *GoBuildConfig) Load() ConfError {

	if _, ok := os.Stat(configPath); ok != nil {
		return ErrorNoConfig
	}

	configBytes, err := ioutil.ReadFile(configPath)
	if err != nil {
		return ErrorUnableToRead
	}

	err = json.Unmarshal(configBytes, conf)
	if err != nil {
		return ErrorUnableToParse
	}

	return ErrorNil
}

// Save Will create a new basic config file
func (conf *GoBuildConfig) Save() error {
	bin, err := json.MarshalIndent(conf, "", "    ")
	if err != nil {
		return err
	}
	return ioutil.WriteFile(configPath, bin, 0600)
}
