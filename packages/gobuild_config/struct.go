package goBuildConfig

// GoBuildConfig is the stuct used to define the layout of the gobuild.json file
type GoBuildConfig struct {
	Application applicationData `json:"application"`
	Tools       toolsData       `json:"tools"`
	Publish     []pubInfo       `json:"publish"`
}

// ## Application ##
type applicationData struct {
	Name    string `json:"name"`
	Version string `json:"version"`
}

// ## Tools ##
type toolsData struct {
	Go toolsGoData `json:"go, omitempty"`
}

// == Go Data ==
type toolsGoData struct {
	Version string `json:"version"`
}

type pubInfo struct {
	Title        string            `json:"title"`
	OutFile      string            `json:"outfile"`
	SysVars      map[string]string `json:"sysvars, omitempty"`
	InstallSufix string            `json:"installsufix, omitempty"`
	PostCmd      string            `json:"postcmd, omitempty"`
}
